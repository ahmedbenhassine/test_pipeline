# -*- coding: utf-8 -*-
"""
Created on Thu Jun 12 09:58:50 2014

@author: tle (tle@keplercheuvreux.com)
"""

class sql_connector:
  
    _PROD_SERVER = 'KECH-DB03:1447'   
    _MATLAB_SERVER = '172.29.53.178:3306'
    _FLEXPORT_SERVER = 'KECH-DB04:1447'
    _MATLAB_LOGIN = {'admin':'Paris4ever!!'}
    _REPOSITORY_DB = 'KGR'
    _MARKET_DATA_DB = 'MARKET_DATA'
    _QUANT_DB = 'QUANT'
    _FLEXPORT_DB = 'FLEX_EXPORT'
    _TIMEOUT = 300 # TIMEOUT en secondes.
  
    def __init__(self):
        from sqlalchemy import create_engine
        self.engine_creator = create_engine
    
    def connection(self,server='production'):
        if server=='production':
            connection_str = "mssql+pymssql://@%s/%s" % (self._PROD_SERVER,self._QUANT_DB) 
            engine = self.engine_creator(connection_str, legacy_schema_aliasing=False, connect_args={'timeout': self._TIMEOUT})
        
        elif server=='Matlab':
            connection_str = "mysql+pymysql://%s:%s@%s" % ('admin','Paris4ever!!',self._MATLAB_SERVER)
            engine = self.engine_creator(connection_str)
        
        elif server=="FLEXPORT":
            connection_str = "mssql+pymssql://@%s/%s" % (self._FLEXPORT_SERVER,self._FLEXPORT_DB)
            engine = self.engine_creator(connection_str, legacy_schema_aliasing=False, connect_args={'timeout': self._TIMEOUT})
        return engine


class my_env:
    read_only_repository = "x:\\kc_repository_new"
    get_tick = "get_tick\\ft"
    
    
