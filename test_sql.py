from db_connexion import sql_connector
connector = sql_connector()
con_mis = connector.connection()

import pandas as pd

def read_sql(query):
    df = pd.read_sql(query, con_mis)
    return df